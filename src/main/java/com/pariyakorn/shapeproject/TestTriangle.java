/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pariyakorn.shapeproject;

/**
 *
 * @author acer
 */
public class TestTriangle {
    public static void main(String[] args) {
    Triangle triangle1 = new Triangle(4,2);
    System.out.println("Area of triangle1 Is " + triangle1.calArea());
    triangle1.setH(2);
    triangle1.setW(7);
    System.out.println("Area of triangle1 (h = " + triangle1.getH() + " w = " + triangle1.getW() + ") Is " + triangle1.calArea());
    triangle1.setH(0);
    triangle1.setW(0);
    System.out.println("Area of triangle1 (h = " + triangle1.getH() + " w = " + triangle1.getW() + ") Is " + triangle1.calArea());
    System.out.println(triangle1.toString()); //add to String
    System.out.println(triangle1); 
    }
   
}
