/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pariyakorn.shapeproject;

/**
 *
 * @author acer
 */
public class Triangle {
    private double h ;
    private double w ;
public Triangle(double h, double w){
        this.h = h;
        this.w = w;
    }
    public double calArea(){
      return 0.5 * h * w ;
    }       
    public double getH(){
          return h;
    }
     public void setH(double h){
         if(h<=0){
             System.out.println("Error: High must more than zero!!!!");
            return;
         }
         this.h = h;
    }
     public double getW(){
          return w;
    }
     public void setW(double w){
         if(w<=0){
             System.out.println("Error: Widht must more than zero!!!!");
            return;
         }
         this.w = w;
    }
    @Override
     public String toString(){
         return "Area of triangle(h = " + this.getH() + " w = " + this.getW() + " ) is " + this.calArea();
     }
}
      


