/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pariyakorn.shapeproject;

/**
 *
 * @author acer
 */
public class TestRectangle {
    public static void main(String[] args) {
        Rectangle rectangle1 = new Rectangle(4,6);
        System.out.println("Area of rectangle1 Is " + rectangle1.calArea());
        rectangle1.setH(2);
        rectangle1.setW(7);
        System.out.println("Area of rectangle1 (h = " + rectangle1.getH() + " w = " + rectangle1.getW() + ") Is " + rectangle1.calArea());
        rectangle1.setH(0);
        rectangle1.setW(0);
        System.out.println("Area of rectangle1 (h = " + rectangle1.getH() + " w = " + rectangle1.getW() + ") Is " + rectangle1.calArea());
        System.out.println(rectangle1.toString()); //add to String
        System.out.println(rectangle1); 
    }
    
}
